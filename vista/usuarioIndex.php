<!DOCTYPE html>
<html>
<head>
    <title>Iniciar sesión</title>
    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css"
    />
    <link
      rel="stylesheet"
      href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css"
    />

    <!-- Bootstrap -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65"
      crossorigin="anonymous"
    />
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4"
      crossorigin="anonymous"
    ></script>
</head>
<body>  
  <div class="container-fluid d-flex flex-column vh-100 bg-gray">
    <div class="d-flex justify-content-end p-4">
      <a href="usuarios.html" class="btn btn-primary m-2">Ir a Usuarios</a>
      <a href="roles.html" class="btn btn-primary m-2">Ir a Roles</a>
    </div>
    <div class="row align-items-center justify-content-center flex-grow-1">
      <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 card p-5 shadow-lg p-3">
        <div class="text-center">
          <h2 class="mt-5">Bienvenido</h2>
          <h3 id="nombreUsuario" class="mt-4"></h3>
          <p id="telefono" class="mt-3"></p>
          <p id="correo" class="mt-3"></p>
          <p id="ultimaconexion" class="mt-3"></p>
        </div>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function() {
      // Obtener los parámetros de la URL
      const urlParams = new URLSearchParams(window.location.search);

      const nombreUsuario = urlParams.get('nombre');
      const telefono = urlParams.get('telefono');
      const correo = urlParams.get('correo');
      const ultimaconexion = urlParams.get('ultimaconexion');

      $('#nombreUsuario').text(nombreUsuario);
      $('#telefono').text(telefono);
      $('#correo').text(correo);
      $('#ultimaconexion').text(ultimaconexion);
            
    });
  </script>
</body>
</html>
