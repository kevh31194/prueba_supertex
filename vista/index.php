<!DOCTYPE html>
<html>
<head>
    <title>Iniciar sesión</title>
    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css"
    />
    <link
      rel="stylesheet"
      href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css"
    />

    <!-- Bootstrap -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65"
      crossorigin="anonymous"
    />
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4"
      crossorigin="anonymous"
    ></script>
</head>
<body>
  <div class="container d-flex align-items-center justify-content-center vh-100">
    <div class="card p-5 shadow-lg p-3">
      <h2 class="text-center mb-4">Iniciar sesión</h2>
      <form id="login-form">
        <div class="form-group mb-4">
          <label for="correoLogin">Correo del usuario</label>
          <input type="text" class="form-control" id="correoLogin" name="correoLogin" required>
        </div>
        <div class="form-group mb-4">
          <label for="password">Contraseña</label>
          <input type="password" class="form-control" id="password" name="password" required>
        </div>
        <button class="btn btn-primary btn-block" id="btnLogin">Iniciar sesión</button>
      </form>
    </div>
  </div>
  <script>
    $(document).ready(function() {
      $('#btnLogin').on('click',function(event) {
        event.preventDefault();

        var correoLogin = $('#correoLogin').val();
        var password = $('#password').val();
                
        $.ajax({
          url: "../controlador/usuarioController.php",
          method: "POST",
          data: {
            action: "login",
            correoLogin: correoLogin, // Cambiado a 'nombre'
            password: password
          },
          success: function(response) {
            // Parsear el JSON
            const data = JSON.parse(response);
            console.log(data);
                      
            const nombre = data.nombre;
            const telefono = data.telefono;
            const correo = data.correo;
            const ultimaconexion = data.ultimaconexion;
            const password = data.password;

            // Codificar los datos en la URL y redirigir
            const queryParams = `nombre=${encodeURIComponent(nombre)}&telefono=${encodeURIComponent(telefono)}&correo=${encodeURIComponent(correo)}&ultimaconexion=${encodeURIComponent(ultimaconexion)}`;
            window.location.href = `../vista/usuarioIndex.php?${queryParams}`;

          }
        });
      });
    });
    </script>
</body>
</html>
