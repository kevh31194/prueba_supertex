<?php
  require_once "conexion.php";

  class Usuario{
    private $conexion;

    function __construct() {
      $db = new Conexion();
      $this->conexion = $db->conectar();
    }

    public function mostrar() {
      $query = "SELECT * FROM usuarios";
      $stmt = $this->conexion->prepare($query);
      $stmt->execute();
      $usuarios = $stmt->fetchAll();

      return $usuarios;
    }

    public function mostrarUsuario($idusuario) {
      $query = "SELECT idusuario, nombre, telefono, correo, ultimaconexion FROM usuarios WHERE idusuario = :idusuario;";
      $stmt = $this->conexion->prepare($query);

      $stmt ->bindParam(":idusuario", $idusuario);

      $stmt->execute();
      $usuarios = $stmt->fetchAll();

      return $usuarios;
    }


    public function insertarUsuarios($nombre, $telefono, $correo, $password) {
      $query = "INSERT INTO usuarios (nombre, telefono, correo, password) VALUES (:nombre, :telefono, :correo, :password)";
      $stmt = $this->conexion->prepare($query);

      //Vincular parametro al nombre de la variable
      $stmt->bindParam(":nombre", $nombre);
      $stmt->bindParam(":telefono", $telefono);
      $stmt->bindParam(":correo", $correo);
      $stmt->bindParam(":password", $password);

      return $stmt->execute();
    }

    public function actualizarUsuario($idusuario, $nombre, $telefono, $correo) {
      $query = "UPDATE usuarios SET idusuario = :idusuario, nombre = :nombre, telefono = :telefono, correo = :correo WHERE idusuario = :idusuario";
      $stmt = $this->conexion->prepare($query);

      //Vincular parametro al nombre de la variable
      $stmt->bindParam(":idusuario", $idusuario);
      $stmt->bindParam(":nombre", $nombre);
      $stmt->bindParam(":telefono", $telefono);
      $stmt->bindParam(":correo", $correo);
      $stmt->bindParam("ultimaconexion", $ultimaconexion);
      
      return $stmt->execute();
    }

    public function eliminarUsuario($idusuario){
      $query = "DELETE FROM `usuarios` WHERE idusuario = :idusuario";
      $stmt = $this->conexion->prepare($query);

      //Vincular parametro al nombre de la variable
      $stmt->bindParam(":idusuario", $idusuario);

      return $stmt->execute();
    }


    //Login
    public function login($correo, $password) {
    try {
        $query = "SELECT `idusuario`, `nombre`, `telefono`, `correo`, `ultimaconexion`, `password` FROM usuarios WHERE correo = :correo;";
        $stmt = $this->conexion->prepare($query);
        $stmt->bindParam(':correo', $correo);
        $stmt->execute();
        
        // Verificar si se encontraron resultados
        if ($stmt->rowCount() > 0) {
            $correoUser = $stmt->fetch(PDO::FETCH_ASSOC);
            
            // Verificar la contraseña
            if ($password == $correoUser['password']) {
                return $correoUser; // Devolver el usuario si la contraseña es correcta
            }
        }

        return false; // Usuario no encontrado o contraseña incorrecta
    } catch (PDOException $e) {
        // Manejar la excepción aquí (puedes imprimir el error o guardar en un registro)
        echo "Error: " . $e->getMessage();
        return false;
    }
}



  }

?>