<?php
  require_once "conexion.php";

  class Rol{
    private $conexion;

    function __construct() {
      $db = new Conexion();
      $this->conexion = $db->conectar();
    }

    public function mostrarRol(){
      $query = "SELECT * FROM roles";
      $stmt = $this->conexion->prepare($query);
      $stmt->execute();
      $roles = $stmt->fetchAll();

      return $roles;  
    }  

    public function verRol($idrol){
      $query = "SELECT `idrol`, `nombre` FROM `roles` WHERE idrol = :idrol;";
      $stmt = $this->conexion->prepare($query);

      //Vincular parametro al nombre de la variable
      $stmt ->bindParam(":idrol", $idrol);

      $stmt->execute();
      $rol = $stmt->fetchAll();

      return $rol;
    }
      
    public function insertarRol($nombre) {
      $query = "INSERT INTO roles (nombre) VALUES (:nombre)";
      $stmt = $this->conexion->prepare($query);
      
      //Vincular parametro al nombre de la variable
      $stmt->bindParam(':nombre', $nombre);
        
      return $stmt->execute();
    }

    public function actualizarRol($idrol, $nombre) {
      $query = "UPDATE roles SET idrol = :idrol, nombre = :nombre WHERE idrol = :idrol";
      $stmt = $this->conexion->prepare($query);
      
      //Vincular parametro al nombre de la variable
      $stmt->bindParam(':idrol', $idrol);
      $stmt->bindParam(':nombre', $nombre);
        
      return $stmt->execute();
    }

    public function eliminarRol($idrol) {
      $query = "DELETE FROM `roles` WHERE idrol = :idrol;";
      $stmt = $this->conexion->prepare($query);

      //Vincular parametro al nombre de la variable
      $stmt->bindParam(":idrol", $idrol);

      return $stmt->execute();
    }
  }
?>