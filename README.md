# prueba_supertex

# Instrucciones para Ejecutar un Proyecto PHP

# Configuración del Proyecto
Clona o Copia los archivos del proyecto en el directorio de documentos raíz de tu servidor web. En Apache, esto generalmente es la carpeta "htdocs" 

# Configuración de la Base de Datos
Importa el Esquema de la Base de Datos el cual tiene el nombre de: "prueba_supertex.sql"

# Configura la Conexión a la Base de Datos:
Ingresa en la carpeta modelo en el archivo conexion configura el host en donde se ejecutara el proyecto, el nombre de la base de datos por "prueba_supertex",
cambia el usuario en donde se ejecutara el proyecto y si tienes una contraseña agregarla en el password.

# Ejecutar el proyecto
Inicia el Servidor Web:
Inicia el servidor web Apache

# Accede al Proyecto en tu Navegador:
Abre tu navegador web y visita la URL correspondiente a tu servidor local. Si estás usando Apache en tu máquina local, la URL podría ser:
http://localhost/prueba_supertex/vista/index.php

# Interactúa con el Proyecto:
Para iniciar sesion puedes utilizar este usuario
Usuario: flor@gmail.com
Contraseña: 1234

Al ingresar se muestran los datos personales nombre, telefono, correo y ultima conexion
En la parte superior derecha estan los botones para ir al CRUD de usuarios y el CRUD de roles con sus respectivos botones
para agregar, modificar y eliminar mediante ventanas modales.

# Diagrama de la base de datos
![Diagrama de la base de datos](ER.png)