<?php
  require_once "../modelo/usuarioModel.php";

  class usuarioController{

    //Mostrar los usuarios
    public function mostrarUsuarios(){
      $mostrarUsuarios = new Usuario;
      $usuarios = $mostrarUsuarios->mostrar();

      //Retornar o imprimir la respuesta como JSON
      echo json_encode($usuarios);
    }

    //Mostrar solo un usuarios
    public function verUsuario($idusuario){
      $mostrarUsuarios = new Usuario;
      $usuarios = $mostrarUsuarios->mostrarUsuario($idusuario);

      //Retornar o imprimir la respuesta como JSON
      echo json_encode($usuarios);
    }

    //Insertar los usuarios
    public function insertarUsuario($nombre, $telefono, $correo, $password){
      $insertarUsuario = new Usuario;
      $resultadoInsertar = $insertarUsuario->insertarUsuarios($nombre,$telefono, $correo, $password);

      if ($resultadoInsertar) {
        echo "<br>Usuario insertado correctamente.";
      } else {
        echo "<br>Error al insertar usuario.";
      }
    }

    //Actualizar los usuarios
    public function actualizarUsuario($idusuario, $nombre, $telefono, $correo){
      $actualizarUsuario = new Usuario;
      $resultadoActualizar = $actualizarUsuario->actualizarUsuario($idusuario, $nombre, $telefono, $correo);

      if ($resultadoActualizar) {
        echo "<br>Usuario actualizado correctamente.";
      } else {
        echo "<br>Error al altualizar usuario.";
      }  
    }

    //Eliminar usuario
    public function eliminarUsuario($idusuario){
      $eliminarUsuario = new Usuario;
      $resultadoEliminar = $eliminarUsuario->eliminarUsuario($idusuario);

      if ($resultadoEliminar) {
        echo "<br>Usuario eliminado correctamente.";
      } else {
        echo "<br>Error al eliminar usuario.";
      }
    }

    public function login($correoLogin, $password) {
    $loginUsuario = new Usuario;
    $resultadoLogin = $loginUsuario->login($correoLogin, $password);

    if ($resultadoLogin !== false) {
      echo json_encode($resultadoLogin);
      // Inicio de sesión exitoso
      session_start();
      $_SESSION['usuario'] = $resultadoLogin;
      exit();
    } else {
      // Inicio de sesión fallido
      // Puedes mostrar un mensaje de error al usuario o realizar otra acción
      echo json_encode(['success' => false, 'message' => 'Inicio de sesión fallido. Verifica tus credenciales.']);
    }
}


}
  //Instancia del controlador y ejecutar la acción correspondiente
  $controller = new usuarioController();

  //Mostrar usuarios y mostrar un solo usuario
  if (isset($_GET["action"])) {
    $action = $_GET["action"];
      
    if ($action === "mostrar") {
      $controller->mostrarUsuarios();
    } elseif ($action === "mostrarUno") {//Mostrar un usuarios      
      $idusuario = $_GET["idusuario"];
      $controller->verUsuario($idusuario);
    }
  }

  //Agregar usuarios, modificar y eliminar usuarios
  if (isset($_POST["action"])) {
    $action = $_POST["action"];
    
    if ($action === "guardar") {
      // Recibir los datos enviados desde la solicitud AJAX
      $nombre = $_POST['nombre'];
      $telefono = $_POST['telefono'];
      $correo = $_POST['correo'];
      $password = $_POST['password'];
      $controller->insertarUsuario($nombre, $telefono, $correo, $password);

    } elseif ($action === "modificar") { //Actualizar Usuario
      // Recibir los datos enviados desde la solicitud AJAX
      $nombre = $_POST['nombre'];
      $telefono = $_POST['telefono'];
      $correo = $_POST['correo'];
      $password = $_POST['password'];
      // Verificar si se proporcionó el ID de usuario
      if (isset($_POST['idusuario'])) {
        $idusuario = $_POST['idusuario'];
            
        // Actualizar usuario existente
        $controller->actualizarUsuario($idusuario, $nombre, $telefono, $correo);
        // Después de realizar la actualizació
        $response = array(
          "status" => "success",
          "message" => "Usuario actualizado correctamente.",
          "idusuario" => $idusuario
        );
        echo json_encode($response);
      }
    } elseif ($action === "eliminar") {//Eliminar Usuario
      // Verificar si se proporcionó el ID de usuario
      if (isset($_POST['idusuario'])) {
        $idusuario = $_POST['idusuario'];
            
        //Eliminar usuario existente
        $controller->eliminarUsuario($idusuario);
        // Después de eliminar
        $response = array(
          "status" => "success",
          "message" => "Usuario actualizado correctamente.",
          "idusuario" => $idusuario
        );
        echo json_encode($response);
      }
    } elseif ($action === "login") {

      if (isset($_POST['correoLogin']) && isset($_POST['password'])) {
        $correoLogin = $_POST['correoLogin'];
        $password = $_POST['password'];
          
        $controller->login($correoLogin, $password);
      }
    }
    }

?>