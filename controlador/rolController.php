<?php
require_once "../modelo/rolModel.php";

class rolController {

  //Funcion para mostrar los roles
  public function mostrarRoles() {
    $mostrar = new Rol;
    $roles = $mostrar->mostrarRol();

    //Retornar o imprimir la respuesta como JSON
    echo json_encode($roles);
  }

  //Mostrar solo un usuarios
    public function verRol($idrol){
      $mostrarRol = new Rol;
      $rol = $mostrarRol->verRol($idrol);

      //Retornar o imprimir la respuesta como JSON
      echo json_encode($rol);
    }
    
  public function insertarRol($nombre) {
    $insertarRol = new Rol;
    $resultadoInsertar = $insertarRol->insertarRol($nombre);

    if ($resultadoInsertar) {
      echo "<br>Rol insertado correctamente.";
    } else {
      echo "<br>Error al insertar rol.";
    }
  }

  public function actualizarRol($idrol, $nombre) {
    $actualizarRol = new Rol;
    $resultadoActualizar = $actualizarRol->actualizarRol($idrol, $nombre);

    if ($resultadoActualizar) {
      echo "<br>Rol actualizado correctamente.";
    } else {
      echo "<br>Error al actualizar rol.";
    }
  }

  public function eliminarRol($idrol) {
    $eliminarRol = new Rol;
    $resultadoEliminar = $eliminarRol->eliminarRol($idrol);

    if ($resultadoEliminar) {
      echo "<br>Rol eliminado correctamente.";
    } else {
        echo "<br>Error al eliminar rol.";
    }
  }
}

  //Instancia del controlador y ejecutar la acción correspondiente
  $controller = new rolController();

  //Mostrar roles y mostrar un solo rol
  if (isset($_GET["action"])) {
    $action = $_GET["action"];
      
    if ($action === "mostrar") {
      $controller->mostrarRoles();
    } elseif ($action === "mostrarUno") {//Mostrar un usuarios      
      $idrol = $_GET["idrol"];
      $controller->verRol($idrol);
    }
  }

  //Agregar, modificar y eliminar roles
  if (isset($_POST["action"])) {
    $action = $_POST["action"];
    
    if ($action === "guardar") {
      // Recibir los datos enviados desde la solicitud AJAX
      $nombre = $_POST['nombre'];
      $controller->insertarRol($nombre);

    } elseif ($action === "modificar") { //Actualizar rol
      // Recibir los datos enviados desde la solicitud AJAX
      $nombre = $_POST['nombre'];
      // Verificar si se proporcionó el ID de rol
      if (isset($_POST['idrol'])) {
        $idrol = $_POST['idrol'];
            
        // Actualizar rol existente
        $controller->actualizarRol($idrol, $nombre);
        // Después de realizar la actualizació
        $response = array(
          "status" => "success",
          "message" => "Rol actualizado correctamente.",
          "idusuario" => $idrol
        );
        echo json_encode($response);
      }
    } elseif ($action === "eliminar") {//Eliminar Rol
      // Verificar si se proporcionó el ID de rol
      if (isset($_POST['idrol'])) {
        $idrol = $_POST['idrol'];
            
        // Actualizar rol existente
        $controller->eliminarRol($idrol);
        // Después de realizar la actualización
        $response = array(
          "status" => "success",
          "message" => "Rol actualizado correctamente.",
          "idusuario" => $idrol
        );
        echo json_encode($response);
      }
    }
  }

?>